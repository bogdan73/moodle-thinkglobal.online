<script>
    function getCourseID(doc_html) {
        const regex1_course_id = /\{"courseid":"(\d+)"/iu; //{"courseid":"536"
        var course_id_match1 = doc_html.match(regex1_course_id);
        console.log(course_id_match1);
        var course_id = course_id_match1[1];
        if (course_id != null) return course_id;

        const regex2_course_id = /\/course\/view\.php\?id=(\d+)/iu;
        var course_id_match2 = doc_html.match(regex2_course_id);
        return course_id_match2[1];
    }

    function getLessonNumber(doc) {
        const title_element = doc.getElementsByTagName("title")[0];
        console.log(title_element.innerHTML);
        const regex = /Урок\s+(\d+)/iu;
        var lesson_match = title_element.innerHTML.match(regex)
        if (lesson_match) return lesson_match[1];
    }

    function getSpreadsheetId(json) {
        for (var j = 0; j < json.table.rows.length; j++) {
            var row = json.table.rows[j]
            if (row['c'][0]['v'] == course_id) {
                return row['c'][1]['v'];
                console.log(spreadsheetId);
                break;
            }
        }
    }

    function getLessonHTML(json, lesson_num) {
        for (var j = 0; j < json.table.rows.length; j++) {
            var row = json.table.rows[j]
            if (row['c'][0]['v'] == lesson_num) return row['c'][1]['v'];
        }
    }

    function getCoursesDocId() {
        return '1Z-hGUiwEYB6Nn7ot89iP5XsUanncLCLaw59fuNYV5KM';
    }

    const courses_doc_id = getCoursesDocId();
    console.log(document.documentElement.innerHTML);
    var course_id = getCourseID(document.documentElement.innerHTML);
    fetch(`https://docs.google.com/spreadsheets/d/${courses_doc_id}/gviz/tq?tqx=out:json`)
        .then(res => res.text())
        .then(text => {
            const json = JSON.parse(text.substr(47).slice(0, -2));
            console.log(text);
            var spreadsheetId = getSpreadsheetId(json);
            fetch(`https://docs.google.com/spreadsheets/d/${spreadsheetId}/gviz/tq?tqx=out:json`)
                .then(res => res.text())
                .then(text => {
                    const json = JSON.parse(text.substr(47).slice(0, -2));
                    var lesson_num = getLessonNumber(document); // lesson_match[1];
                    var lesson_html = getLessonHTML(json, lesson_num);
                    var divContainer = document.getElementById("lesson_html");
                    divContainer.innerHTML = lesson_html;
                })
        })
</script>
<div id="lesson_html"></div>
